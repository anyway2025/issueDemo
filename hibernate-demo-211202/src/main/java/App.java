import entity.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

/**
 * @author liuchuanwei.ex
 * @date 2021/12/2
 */
public class App {
    private static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    public static void main(String[] args) {
        Long id = 201L;
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Order order = (Order) session.get(Order.class, id);

        System.out.println(order.getOrderItemList().size());

        session.getTransaction().commit();
    }
}
